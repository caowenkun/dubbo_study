package com.alibaba.dubbo.demo.provider;

import com.alibaba.dubbo.demo.DemoService;

public class DemoServiceImpl implements DemoService {
	
	public DemoServiceImpl() {
		System.out.println("Provider1 DemoServiceImpl init");
	}
	
	
	public String sayHello(String name) {
		System.out.println("Provider1 is runing......");
		return "Provider1: welcome [" + name +"]";
	}
}
