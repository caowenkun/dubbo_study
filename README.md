Dubbo 学习
===================================
Dubbo 基础使用方法,以及简单的负载测试。我直接将eclipse workspace提交，另外还有三个tomcat和zookeeper

文件目录介绍
---------------------------------------------------------------
Consumer 编写了一个简单主界面用于测试<br /> 
Provider1,Provider2 是基本一样的服务提供者工程(区别仅仅是DemoServiceImpl中System.out日志做了区分，以便观察)。注意两个provider.xml中的dubbo:protocol端口不能相同<br /> 
zookeeper-3.4.5 作为服务注册中心<br /> 
tomcat-c  （修改了端口为7001）发布Consumer<br /> 
tomcat-p1 （修改了端口为7002）发布Provider1<br /> 
tomcat-p2  （修改了端口为7003）发布Provider2<br /> 

### 相关链接  
1.[Dubbo官方主页](http://code.alibabatech.com/wiki/display/dubbo/Home-zh)<br />  
2.[关于zookeeper介绍](http://www.ibm.com/developerworks/cn/opensource/os-cn-zookeeper/)<br />  

###其他
tomcat\bin 下放了一个cmd.link；例如tomcat-c\bin下的c.lnk；开启一个cmd窗口，然后输入startup.bat。以便区分打开的多个tomcat控制台窗口

###代码内容
使用官方提供的Consumer,Provider示例; 编写了一个index.jsp以及Servlet，界面输入数据后，在Servlet中通过Dubbo请求后端服务。<br /> 
因为同时启动的2个Provider，Dubbo按照默认的负载均衡方式（详见官方文档-用户指南-负载均衡）调用其中一个。<br /> 
界面在GET方式的表单中输入测试字符串，然后不断刷新结果页面，可看出负载效果<br /> 

###Quick Start
1. 运行zookeeper-3.4.5/bin/zkServer.cmd
2. 运行tomcat-c/bin/c.lnk
3. 运行tomcat-p1/bin/p1.lnk
4. 运行tomcat-p2/bin/p2.lnk
5. 浏览器输入http:\\localhost:7001\Consumer
6. 在第一个GET表单中输入 test; 点击提交;看结果页面（然后可不断刷新）