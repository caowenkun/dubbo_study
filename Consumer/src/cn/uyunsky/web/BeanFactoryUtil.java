package cn.uyunsky.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanFactoryUtil implements ApplicationContextAware {

	private static Log log = LogFactory.getLog(BeanFactoryUtil.class);
	private static ApplicationContext applicationContext;

	public void setApplicationContext(ApplicationContext context) throws BeansException {
		if (applicationContext != null) {
			throw new IllegalStateException("ApplicationContextHolder already holded 'applicationContext'.");
		}
		applicationContext = context;
		log.info("holded applicationContext,displayName:" + applicationContext.getDisplayName());
	}

	public static ApplicationContext getApplicationContext() {
		if (applicationContext == null) {
			synchronized (BeanFactoryUtil.class) {
				if (applicationContext == null) {
					//当spring开始加载后,通过ApplicationContextAware将context注入进来
					new ClassPathXmlApplicationContext("classpath:/spring/*.xml");
				}
			}
		}
		return applicationContext;
	}

	public static Object getBean(String beanName) {
		return getApplicationContext().getBean(beanName);
	}

	public static boolean containsBean(String beanName) {
		return getApplicationContext().containsBean(beanName);
	}
}
