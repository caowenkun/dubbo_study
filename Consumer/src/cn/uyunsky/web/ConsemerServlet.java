package cn.uyunsky.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.dubbo.demo.DemoService;

/**
 * Servlet implementation class ConsemerServlet
 */
public class ConsemerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ConsemerServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doGet");
		execute(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doPost");
		execute(request, response);

	}

	private void execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String reqmsg = request.getParameter("msg");
		DemoService demoService = (DemoService) BeanFactoryUtil
				.getBean("demoService"); // 获取远程服务代理

		System.out.println("request:" + reqmsg);
		String result = demoService.sayHello(reqmsg); // 执行远程方法
		System.out.println("response:" + result);

		request.setAttribute("MSG", result);
		request.getRequestDispatcher("result.jsp").forward(request, response);
	}

}
