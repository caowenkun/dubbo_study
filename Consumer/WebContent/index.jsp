<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Consumer</title>
</head>
<body>
<p>GET (get提交中文会有乱码问题,这里输入英文测试.跳转页面后可以不断按F5看负载效果)</p>
<form method="get" action="ConsemerServlet">
	请输入: <input id="msg1" name="msg" type='text'>
	<button type="submit">提交</button>
</form>

<hr>

<p>POST (POST提交中文乱码已处理,这里可输入中文测试.不过跳转页面后按F5自然你应该知道是啥情况)</p>
<form method="post" action="ConsemerServlet">
	请输入: <input id="msg2" name="msg" type='text'>
	<button type="submit">提交</button>
</form>

</body>
</html>