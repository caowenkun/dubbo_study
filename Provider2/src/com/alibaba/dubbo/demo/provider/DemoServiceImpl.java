package com.alibaba.dubbo.demo.provider;

import com.alibaba.dubbo.demo.DemoService;

public class DemoServiceImpl implements DemoService {
	
	public DemoServiceImpl() {
		System.out.println("Provider2 DemoServiceImpl init");
	}
	
	public String sayHello(String name) {
		System.out.println("Provider2 is runing......");
		return "Provider2: welcome [" + name +"]";
	}
}
